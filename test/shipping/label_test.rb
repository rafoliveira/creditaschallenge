require 'test_helper'

class Shipping::LabelTest < Minitest::Test
  def test_data
    { product: TestData::Product::Book }
  end

  def test_is_valid
    product = test_data[:product].call

    assert Shipping::Label.new(product).valid?

    product.stub(:valid?, false) do
      refute Shipping::Label.new(product).valid?
    end
  end

  def test_code
    product = test_data[:product].call

    shipping_label = Shipping::Label.new(product)

    assert_empty shipping_label.code

    assert shipping_label.save

    refute_empty shipping_label.code

    product.stub(:valid?, false) do
      refute Shipping::Label.new(product).save
    end
  end
end
