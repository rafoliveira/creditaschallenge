require 'test_helper'

class OrderItemTest < Minitest::Test
  def test_data
    { book: TestData::Product::Book, order: TestData::Order::Build }
  end

  def test_initialization_constraints
    assert_raises ArgumentError do
      product = test_data[:book].call

      OrderItem.new product: product
    end

    assert_raises ArgumentError do
      order = test_data[:order].call

      OrderItem.new order: order
    end
  end

  def test_order
    product, order = test_data.values.map(&:call)

    order_item = OrderItem.new product: product, order: order

    assert_equal order, order_item.order
  end

  def test_product
    product, order = test_data.values.map(&:call)

    order_item = OrderItem.new product: product, order: order

    assert_equal product, order_item.product
  end

  def test_total
    product, order = test_data.values.map(&:call)

    order_item = OrderItem.new product: product, order: order

    assert_equal 10, order_item.total
  end

  def test_customer
    product, order = test_data.values.map(&:call)

    order_item = OrderItem.new product: product, order: order

    assert_equal order.customer, order_item.customer
  end

  def test_paid_order
    product, order = test_data.values.map(&:call)

    order.stub(:paid?, true) do
      order_item = OrderItem.new product: product, order: order

      assert order_item.paid_order?
    end
  end
end
