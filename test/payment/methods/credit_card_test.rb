require 'test_helper'

class Payment::Methods::CreditCardTest < Minitest::Test
  def test_fetch_by_hashed
    assert_raises ArgumentError do
      Payment::Methods::CreditCard.fetch_by_hashed()
    end

    credit_card = Payment::Methods::CreditCard.fetch_by_hashed('foo')

    assert_instance_of Payment::Methods::CreditCard, credit_card
  end
end
