require 'test_helper'

class Payment::InvoiceTest < Minitest::Test
  def test_data
    { address: TestData::Address::Build, order: TestData::Order::WithProduct }
  end

  def test_initialization_constraints
    assert_raises(ArgumentError) { Payment::Invoice.new }
    assert_raises(ArgumentError) { Payment::Invoice.new order: nil }
    assert_raises(ArgumentError) { Payment::Invoice.new billing_address: nil }
    assert_raises(ArgumentError) { Payment::Invoice.new shipping_address: nil }
  end

  def test_billing_address
    address = test_data[:address].call

    invoice =
      Payment::Invoice.new(order: nil, billing_address: address, shipping_address: nil)

    assert_equal address, invoice.billing_address
  end

  def test_shipping_address
    address = test_data[:address].call

    invoice =
      Payment::Invoice.new(order: nil, billing_address: nil, shipping_address: address)

    assert_equal address, invoice.shipping_address
  end

  def test_order
    invoice =
      Payment::Invoice.new(order: 'order', shipping_address: nil, billing_address: nil)

    assert_equal 'order', invoice.order
  end

  def test_is_valid
    invalid_invoice =
      Payment::Invoice.new(order: nil, shipping_address: nil, billing_address: nil)

    refute invalid_invoice.valid?

    address, order = test_data.values.map(&:call)

    invoice =
      Payment::Invoice.new order: order,
                  shipping_address: address,
                  billing_address: address

    assert invoice.valid?
  end
end
