require 'test_helper'

class PaymentTest < Minitest::Test
  def test_data
    {
      credit_card: TestData::PaymentMethod::CreditCard,
      invoice: TestData::PaymentInvoice::Build,
      order: TestData::Order::WithProduct
    }
  end

  def test_authorization_number
    credit_card = test_data[:credit_card].call

    assert_nil Payment.new(payment_method: credit_card).authorization_number

    payment =
      Payment.new(payment_method: credit_card, authorization_number: 123321)

    assert_equal 123321, payment.authorization_number
  end

  def test_amount
    credit_card = test_data[:credit_card].call

    payment = Payment.new(amount: 100, payment_method: credit_card)

    assert_equal 100, payment.amount
  end

  def test_invoice
    invoice = test_data[:invoice].call
    credit_card = test_data[:credit_card].call

    payment = Payment.new(invoice: invoice, payment_method: credit_card)

    assert_equal invoice, payment.invoice
  end

  def test_order
    order = test_data[:order].call
    credit_card = test_data[:credit_card].call

    payment = Payment.new(order: order, payment_method: credit_card)

    assert_equal order, payment.order
  end

  def test_payment_method
    credit_card = test_data[:credit_card].call

    payment = Payment.new(payment_method: credit_card)

    assert_equal credit_card, payment.payment_method
  end

  def test_is_paid
    credit_card = test_data[:credit_card].call

    payment = Payment.new(payment_method: credit_card)

    refute payment.paid?
  end

  def test_pay
    credit_card = test_data[:credit_card].call

    payment = Payment.new(payment_method: credit_card)

    assert_raises ArgumentError do
      payment.pay
    end

    refute payment.pay(Time.now)
  end
end
