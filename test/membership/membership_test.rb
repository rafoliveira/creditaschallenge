require 'test_helper'

class MembershipTest < Minitest::Test
  def test_data
    { order_item: TestData::OrderItem::Build }
  end

  def test_initialization_constraints
    assert_raises ArgumentError do
      Membership.new
    end
  end

  def test_order
    order_item = test_data[:order_item].call

    assert_equal order_item.order, Membership.new(order_item).order
  end

  def test_order_item
    order_item = test_data[:order_item].call

    assert_equal order_item, Membership.new(order_item).order_item
  end

  def test_customer
    order_item = test_data[:order_item].call

    assert_equal order_item.order.customer, Membership.new(order_item).customer
  end

  def test_is_activated
    order_item = test_data[:order_item].call

    refute Membership.new(order_item).activated?

    refute Membership.new(order_item, activated: true).activated?

    order_item.order.stub(:paid?, true) do
      assert Membership.new(order_item, activated: true).activated?
    end
  end
end
