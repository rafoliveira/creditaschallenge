module TestData
  module Customer
    Build = -> { ::Customer.new }
  end

  module PaymentMethod
    CreditCard = -> { ::Payment::Methods::CreditCard.new }
  end

  module Address
    Build = -> (zipcode = '03178080') { ::Address.new(zipcode: zipcode) }
  end

  module Product
    Build = -> (name, type) { ::Product.new(name: name, type: type) }

    Book = -> (name: 'Awesome book') { Build.(name, :book) }
  end

  module Order
    Build = -> (product = nil, address = nil, customer = nil) do
      addr = address || Address::Build.call
      cust = customer || Customer::Build.call

      ::Order
        .new(cust, address: addr)
        .tap { |order| order.add_product(product) if product }
    end

    WithProduct = -> (options = {}) do
      options[:product] ||= Product::Book.call

      Build.call(*options.values_at(:product, :address, :customer))
    end
  end

  module OrderItem
    Build = -> do
      Order::WithProduct.call.items[0]
    end
  end

  module PaymentInvoice
    Build = -> (address = Address::Build.call, order = nil) do
      ::Payment::Invoice.new shipping_address: address,
                             billing_address: address,
                             order: Order::Build.call(nil, address, nil)
    end
  end
end
