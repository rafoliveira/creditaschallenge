class Resultable
  attr_reader :value

  def initialize(success, value)
    @success = success
    @value = value
  end

  def success?
    @success == true
  end

  def failure?
    !success?
  end

  module Mixin
    def Success(value)
      Resultable.new(true, value)
    end

    def Failure(value)
      Resultable.new(false, value)
    end
  end
end
