class Payment
  module Operations
    class PayOrder
      include ::Resultable::Mixin

      def initialize(order, payment_method:)
        @order = order
        @payment_method = payment_method
      end

      def call(paid_at: Time.now)
        payment = pay_order(paid_at)

        payment.paid? ? Success(payment) : Failure(payment)
      end

      private

      def pay_order(paid_at)
        Payment.new(payment_data).tap do |payment|
          @order.close(payment) if payment.pay(paid_at)
        end
      end

      def payment_data
        {
          authorization_number: Time.now.to_i,
          payment_method: @payment_method,
          invoice: build_invoice,
          amount: @order.total_amount,
          order: @order
        }
      end

      def build_invoice
        Payment::Invoice.new order: @order,
                             billing_address: @order.address,
                             shipping_address: @order.address
      end
    end
  end
end
