class Payment
  class Invoice
    attr_reader :billing_address, :shipping_address, :order

    def initialize(billing_address:, shipping_address:, order:)
      @billing_address = billing_address
      @shipping_address = shipping_address
      @order = order
    end

    def valid?
      (billing_address.is_a?(::Address) && billing_address.valid?) &&
      (shipping_address.is_a?(::Address) && shipping_address.valid?) &&
      (order.is_a?(::Order) && order.valid?)
    end
  end
end
