class OrderItem
  include Delegatable

  delegate :paid?, to: :order, as: :paid_order?
  delegate :customer, to: :order

  attr_reader :order, :product

  def initialize(order:, product:)
    @order = order
    @product = product
  end

  def total
    10
  end
end
