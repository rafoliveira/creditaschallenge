class Membership
  include Delegatable

  delegate :paid_order?, :order, :customer, to: :order_item

  attr_reader :order_item

  def initialize(order_item, overrides = {})
    @order_item = order_item
    @activated = paid_order? && overrides.fetch(:activated, false)
  end

  def activated?
    @activated
  end
end
