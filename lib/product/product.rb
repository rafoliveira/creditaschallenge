class Product
  require_relative 'types'

  attr_reader :name, :type

  def initialize(name:, type:)
    @name, @type = String(name), Types[type]
  end

  def valid?
    !name.empty? && !type.nil?
  end

  Types::ALL.each do |valid_type|
    define_singleton_method("as_#{valid_type}") do |name:|
      Product.new(name: name, type: valid_type)
    end

    define_method("#{valid_type}?") { valid_type == type }
  end
end
