class Product
  module Types
    ALL = [
      BOOK = 'book',
      DIGITAL = 'digital',
      PHYSICAL = 'physical',
      MEMBERSHIP = 'membership'
    ].freeze

    def self.[](value)
      input = String(value).downcase

      return input if ALL.find { |valid_type| valid_type == input }

      raise TypeError, "Invalid type: #{input}, please use one of these #{ALL}"
    end
  end
end
