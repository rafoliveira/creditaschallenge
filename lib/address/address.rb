class Address
  attr_reader :zipcode

  def initialize(zipcode:)
    @zipcode = String(zipcode)
  end

  def valid?
    !zipcode.empty?
  end
end
