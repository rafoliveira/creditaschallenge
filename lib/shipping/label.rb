require 'securerandom'

module Shipping
  require_relative 'operations/generate_label'

  class Label
    include Delegatable

    delegate :valid?, to: :product

    attr_reader :code, :product

    def initialize(product)
      @code = ''
      @product = product
    end

    def save
      return false unless valid?

      @code = SecureRandom.uuid

      true
    end
  end
end
