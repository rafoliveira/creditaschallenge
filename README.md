# Creditas backend challenge

## Agradecimentos
Obrigado pela oportunidade em participar do processo seletivo. Espero que essa experiência agregue a todos os envolvidos. ;)

## Requisitos de ambiente

### Principais dependências

* Ruby >= 2.5.0
* [minitest](https://rubygems.org/gems/minitest)

### Instalação e configuração

1. Instale o Ruby. Exemplo com [rvm](https://rvm.io/rvm/install): `rvm install 2.5.1`
2. Instale o Bundler. `gem install bundler --no-ri --no-rdoc`
3. Faça clone do projeto. `git clone https://gitlab.com/serradura/creditaschallenge.git`
4. Acesse o projeto. Ex: `cd creditaschallenge`
5. Execute `bundle`.

### Execução dos testes

```
  rake
```

**PS:** Aplico o conceito/approach [Let's not](https://robots.thoughtbot.com/lets-not) na suíte de testes.

### Verificação da saúde do projeto

```
  rake notes
```

O comando acima listará comentários que comecem com TODO ou FIXME.

**PS:** Acesse a tag v0.0 para visualizar exemplos de como utilizei após identificar alguns pontos na implementação por conta da criação do testes.

---

## Sobre o desenvolvimento deste projeto

[Link](https://github.com/Creditas/challenge/blob/master/backend/README.md) para o escopo/desafio.

### Funcionalidade

A principal funcionalidade pode ser observada ao executar o comando `ruby bootstrap.rb`, o output representa cada uma das regras de fluxo de pagamento/envio. Que são:

#### Item físico
> Se o pagamento for para um item físico, você deverá gerar um shipping label para o mesmo ser colocado na caixa do envio;

```
#  1 # ***********************
#  2 # Physical product sample
#  3 # ***********************
#  4 #
#  5 # "result.success? == true"
#  6 # "payment.paid? == true"
#  7 # "payment.order.closed? == true"
#  8 #
#  9 #     "product.type == 'physical'"
# 10 #         #<Shipping::Label:0x00007f @code="...",@product=#<Product:>>
```

Explicação:
* A linhas de 1 a 3 é o título do exemplo.
* Linha 5, representa se houve ou não sucesso em toda operação.
* Linha 6, representa se o pagamento foi realizado.
* Linha 7, representa se o pedido foi encerrado.
* Linha 9, apresenta o tipo do produto (Item físico).
* Linha 10, representa a etiqueta gerada para o envio do item.

#### Item de assinatura de serviço
> Caso o pagamento seja uma assinatura de serviço, você precisa ativar a assinatura, e notificar o usuário através de e-mail sobre isto;

```
#  1 # *****************
#  2 # Membership sample
#  3 # *****************
#  4 #
#  5 # "result.success? == true"
#  6 # "payment.paid? == true"
#  7 # "payment.order.closed? == true"
#  8 #
#  9 #     "product.type == 'membership'"
# 10 #         #<Membership:0x00007f98038a17e8... @activated=true>
# 11 #         "Barlano, sua assinatura foi ativada com sucesso."
```

Explicação:
* Linha 9, apresenta o tipo do produto (Item de assinatura).
* Linha 10, representa que uma assinatura foi ativada e associada ao comprador (Membership é associado ao Customer).
* Linha 11, representa que uma notificação foi gerada para comunicar o comprador.

#### Livro
> Caso o pagamento seja um livro comum, você precisa gerar o shipping label com uma notificação de que trata-se de um item isento de impostos conforme disposto na Constituição Art. 150, VI, d.

```
#  1 # ***********
#  2 # Book sample
#  3 # ***********
#  4 #
#  5 # "result.success? == true"
#  6 # "payment.paid? == true"
#  7 # "payment.order.closed? == true"
#  8 #
#  9 #     "product.type == 'book'"
# 10 #         #<Shipping::Label:0x00007 @code="...", @product=#<Product>>
# 11 #         "Pagamento realizado com sucesso!\n\nO livro #<Product: @name=\"Awesome book\" ...> foi separado para envio.\n\nObservação:\nEsse produto trata-se de um item isento de impostos conforme disposto na Constituição Art. 150, VI, d.\n"
```

Explicação:
* Linha 9, apresenta o tipo do produto (Livro).
* Linha 10, representa a etiqueta gerada para o envio do item.
* Linha 11, representa que uma notificação foi gerada para comunicar o comprador (citando a isenção do imposto segundo artigo da constituição).

#### Mídia digital
> Caso o pagamento seja de alguma mídia digital (música, vídeo), além de enviar a descrição da compra por e-mail ao comprador, conceder um voucher de desconto de R$ 10 ao comprador associado ao pagamento.

```
#  1 # **********************
#  2 # Digital product sample
#  3 # **********************
#  4 #
#  5 # "result.success? == true"
#  6 # "payment.paid? == true"
#  7 # "payment.order.closed? == true"
#  8 #
#  9 #     "product.type == 'digital'"
# 10 #         #<Customer:0x.. @name="Bazlano", @discounts=[10]...>
# 11 #        "Bazlano, sua compra realizada com sucesso.\"\n\nAcesse este <a>link</a>, para realizar o download do #<Product:0x00007f98038a0370 @name=\"Awesome video\", @type=\"digital\">.\n\nObservação:\nPor conta deste item você possui um total de R$ 10 de desconto para as próximas compras.\n"
```

Explicação:
* Linha 9, apresenta o tipo do produto (Mídia digital).
* Linha 10, apresenta que o cliente tem um valor de desconto.
* Linha 11, representa que uma notificação foi gerada para comunicar o comprador (citando que o comprador adquiriu um desconto para ser usado em futuras compras).

---

**PS:** O último cenário de exemplo gerado pelo `ruby bootstrap.rb` é randômico, podendo adicinar um ou todos os tipos de itens em um mesmo pedido.


### Design/Arquitetura

Meu objetivo foi de entregar uma implementação que atendesse a quatro requisitos: Código *expressivo*, *previsível*, *sequencialmente lógico* e *modular*.

**Modularidade e expressividade**

Tentei alcançar os atributos deste tópico a começar pela organização dos diretórios que representam os conceitos/módulos que compõem o domínio da aplicação. Salvo o diretório `utils` que concentra utilitários para os demais componentes do sistema.
```
lib
├── address
├── customer
├── membership
├── notification
├── order
├── payment
├── product
├── shipping
└── utils
```

Visão geral dos subdiretórios (exceto `utils`) por responsabilidade:
```
lib
├── address
├── customer
│   └── operations
├── membership
├── notification
│   └── operations
├── order
│   └── operations
├── payment
│   ├── invoice
│   ├── methods
│   └── operations
├── product
├── shipping
│   └── operations
```

**Subdiretórios `operations`:**
```
lib
├── customer
│   └── operations
├── notification
│   └── operations
├── order
│   └── operations
├── payment
│   └── operations
├── shipping
│   └── operations
```
Estes módulos concentram os pontos de side-effect do sistema, os mesmos podem ser encarados como *use cases*, *commands*. Outra característica importante: Possuem apenas uma única responsabilidade.

**Demais subdiretórios:**
```
lib
├── address
├── customer
├── membership
├── notification
├── order
├── payment
│   ├── invoice
│   ├── methods
├── product
├── shipping
```
Concentram a representação das regras de negócio, esses módulos em sua grande maioria são "puros", ou seja, preferem transformar o estado ao invés de modificá-los. Outro ponto importante, foi o uso de namespaces para agrupar conceitos (vide `payment/invoice` e `payment/methods`).

Tanto na organização quanto nas implementações, foquei em criar abstrações coesas e com responsabilidades específicas. SRP, OCP e DI foram perseguidos mas não ao ponto de complicar a implementação das funcionalidades.

**Acoplamento estável por conta da modularização**

Além do uso dos namespaces, quero evidenciar a implementação do módulo `Product::Types`.

```ruby
# Removi quebras de linha para condensar o exemplo.
class Product::Types
  ALL = [
    BOOK = 'book',
    DIGITAL = 'digital',
    PHYSICAL = 'physical',
    MEMBERSHIP = 'membership'
  ].freeze

  def self.[](value)
    input = String(value).downcase
    return input if ALL.find { |valid_type| valid_type == input }
    raise TypeError, "Invalid type: #{input}, please use one of these #{ALL}"
  end
end
```

Possibilidades por conta da abstração acima:
```ruby
# Usar constantes como referência forte, ex:
Product::Types::BOOK

# Lançar erros caso o tipo seja inválido, ex:
Product::Types['book']
# => "book"

Product::Types['foo']
# => TypeError: Invalid type: foo, please use one of these ["book", "digital", "physical", "membership"]
#from creditaschallenge/lib/product/types.rb:15:in `[]'

# A estratégia acima é usada no construtor da classe Product.
```

**Código sequencialmente lógico**

As principais operações do sistema são:
1. `Payment::Operations::PayOrder`
2. `Order::Operations::SubmitIfPaid`

Sendo que uma sequência já é expressa na ordem acima, os pedidos precisam estar pagos para então serem enviados.

A seguir irei listar outros exemplos relacionados ao tópico:

**Exemplo: Composição das operações para enviar pedidos pagos**
```ruby
class Order::Operations::SubmitIfPaid
  include ::Resultable::Mixin

  OPERATIONS = [
    Shipping::Operations::GenerateLabel,
    Customer::Operations::ActivateMembership,
    Customer::Operations::GrantDiscountForDigitalProduct,
    Notification::Operations::SendEmail
  ].freeze
  #...
end
```

A constante `OPERATIONS` define a ordem de execução e quais são os diferentes tipos de operações que poderão ou não serem executadas (*Chain of Responsibility*) de acordo com os items do pedido.

**Exemplo: Renderização dos fluxos de pagamento/regras de envio (bootstrap.rb)**

```ruby
# RUBY_VERSION >= 2.6
BuildOrder
  .call(customer: Customer.new('Foolano'), product: book)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)

# RUBY_VERSION >= 2.5
BuildOrder
  .call(customer: Customer.new('Foolano'), product: book)
  .yield_self(&PayOrderWithCreditCard)
  .yield_self(&PrintOrderPaymentResult)
  .yield_self(&SubmitOrderIfItWasPaid)
  .yield_self(&TryToPrintTheOrderSubmissionResult)
```

Leitura do exemplo acima:
1. Crie um pedido.
2. Então pague-o com cartão de crédito.
3. Então imprima o resultado o resultado do pagamento.
4. Então envie o pedido se o mesmo estiver pago.
5. Então tente imprimir o resultado do envio do pedido.

**Previsibilidade**

Dada as preferências por transformar o estado mais que modificá-lo e por favorecer o uso de composição, passar a garantir o input e a integridade do estado são pré-requisitos para se obter um correto e previsível output.

A seguir, listo exemplos de como abordei este tópico.
* Evitar o acoplamento de regras de negócios e condicioais a `nil`.
* Validar o estado (Ex: `def valid?`)
* Falhar rápido e com contexto em caso de dados inválidos (Ex: `Product::Types[]` e `Order#add_product`)
* Estabelecer um contrato para encapsular e representar o resultado das operations (Ex: Resultable)

### Possíveis melhorias

Pontos pessoais:
1. Remover fragilidade na ordem de execução da classe Order::Operations::SubmitIfPaid. Como? Isolando/compondo cada fluxo em classes (Strategy) para enviar cada tipo de item.
2. Separar estratégias de envio de email e templates em módulos distintos (Notification::Operations::SendEmail).
3. Testar todas as operations. Não o fiz por conta do uso das mesmas para representar os fluxos de envio de pedidos (bootstrap.rb).
4. Remover o endereço default do construtor da classe Order (Ex: `overrides.fetch(:address) { Address.new(zipcode: '45678-979') }`)
5. Criaria um utils/operation.rb para estabelecer uma base/contrato para criação de operations.
6. Criaria um novo utilitário para representar a verificação de instâncias válidas (Ex: `address.is_a?(::Address) && address.valid?`)

Itens visando o trabalho em time:
1. Organização dos módulos e classes
2. Nomeação de classes e métodos

### Plus (branch `ruby2.6`)

A principal mudança deste branch (`git checkout ruby2.6`) é que substituo `.yield_self` por `.then`, confira. :)
